#!/bin/bash

# Holy fuck this was such a headache to figure out. So I wrote a script.
if [ -z "$1" ]; then
  echo "Usage: ./chgdm-bg <path/to/picture.png> [y/n]"
  exit 1
fi

echo MAKE SURE YOUR PICTURE IS THE SAME RESOLUTION AS YOUR SCREEN.
echo If you dont you will likely regret it.
sleep 3

if [ ! -f $1 ]; then
  echo Error: could not find file $1
  exit 1
fi

PIC=$1
WORKDIR=${HOME}/chgdm-bg-tmp
DATE=$(date +"%Y-%m-%d_%T")

if [ ! -d $WORKDIR ]; then
  mkdir -p $WORKDIR/theme
fi

cd "${WORKDIR}/theme"
mkdir -p backups

IFS='/' read -r -a array <<< "$PIC"
cp $PIC .
PIC=${array[-1]}
chmod 777 $PIC

SHARE=/usr/share/gnome-shell/
GRESOURCE=gnome-shell-theme.gresource
CSS=gnome-shell.css
XMLRES=gnome-shell-theme.gresource.xml

if [ -f ${SHARE}${GRESOURCE} ]; then
  echo Backing up ${SHARE}${GRESOURCE}...
  cp ${SHARE}${GRESOURCE} "backups/${GRESOURCE}.bak.${DATE}"
  cp ${SHARE}${GRESOURCE} $GRESOURCE
  printf "Unpacking binary resource..."
  for r in `gresource list $GRESOURCE`; do
    printf "."
    gresource extract $GRESOURCE $r >$WORKDIR/${r#\/org\/gnome\/shell/}
  done
  echo
  WORKDIR="${WORKDIR}/theme/"
  

  if [ -f ${XMLRES} ]; then
    echo Backing up ${XMLRES}...
    cp "${XMLRES}" "backups/${XMLRES}.bak.${DATE}"
    if [ -f "tmp" ]; then
      rm -f tmp
    fi
    touch tmp
    awk -v var="$PIC" '/gresource prefix="\/org\/gnome\/shell\/theme">/ { print; printf "    <file>"; printf var; print "<\/file>"; next }1' $XMLRES > tmp && mv tmp $XMLRES
    #sed -n -i "/\ \ \ \ <file>$PIC<\/file>/d" $XMLRES
    #sed -n -i "/gresource prefix=\"\/org\/gnome\/shell\/theme\">/a \ \ \ \ <file>$PIC<\/file>" $XMLRES
  else
    echo Error: could not find file ${XMLRES}
    exit 1
  fi

  if [ -f $CSS ]; then
    echo Backing up $CSS...
    cp $CSS "backups/${CSS}.bak.${DATE}"
    sed -i 's/#lockDialogGroup/#lockDialogGroup-old/' $CSS
    printf "#lockDialogGroup {\n    background: #2e3436 url(${PIC});\n    background-repeat: no-repeat;\n    background-size: cover; }\n" >> $CSS
  else
    echo Error: could not find file $CSS
    exit 1
  fi

  echo Compiling binary resources...
  glib-compile-resources $XMLRES
  echo Copying binary resources over to ${SHARE}...
  sudo cp $GRESOURCE "${SHARE}${GRESOURCE}"
  sudo cp $CSS "${SHARE}${CSS}"
  
  if [ -z $2 ]; then
    echo "Restart now? (y/n)"
    read ans
  fi

  if [ "$2" = "y" ] || [ "$ans" = "y" ]; then
    echo Restarting...
    for i in {1..5}; do
      echo $((6-i))
      sleep 1
    done
    #/usr/bin/gnome-session-quit --no-prompt
    #sudo pkill -u $USER
    sudo shutdown -r now
  else
    echo Restart to load your changes.
  fi
else
  echo Error: could not find file ${SHARE}${GRESOURCE}
  exit 1
fi

